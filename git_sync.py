#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 20:26:26 2019

@author: bipin
"""

import git
import os

def create_dummy_repo(git_dir1):
    if not os.path.exists(git_dir1):
        os.mkdir(git_dir1)
        file1 = open(os.path.join(git_dir1,'test.txt'))
        file1.write("this is the initial message\n")
        file1.flush()
        file1.close()
        repo1 = git.Repo.init(git_dir1)
        repo1

def clone_repos(git_dir1,git_dir2):
    repo1 = git.Repo(git_dir1) 
    if not os.path.exists(git_dir2):
        repo1.clone(git_dir2)

def update_git_dirs(git_dir1,git_dir2,archive_file):
    if not os.path.exists(git_dir2):
        clone_repos(git_dir1,git_dir2)
    if not os.path.exists(git_dir1):
        clone_repos(git_dir2,git_dir1)        
    repo1 = git.Repo(git_dir1)
    repo2 = git.Repo(git_dir2)
    if repo1.commit() != repo2.commit():
        print('pulling')
        repo1.git.pull(git_dir2)
        repo2.git.pull(git_dir1)
        repo1.archive(open(archive_file,'wb'),format = 'tar.gz')

if __name__ == '__main__': 
    dir1 = r'/mnt/removable/gits'
    dir2 = r'/home/user/projects/gits'
    dir3 = r'/home/user/projects/archive'
    project_name_list = ['repo1',
                         'repo2',
                         'repo3',
                         ]
    git_list1 = []
    for project_name in project_name_list:
        git_list1.append((os.path.join(dir1,project_name),
                          os.path.join(dir2,project_name),
                          os.path.join(dir3,project_name+'.tar.gz')) )
        
        
    for git_dir1,git_dir2,archive_file in git_list1:
        print("processing :{0}".format(git_dir1))
        update_git_dirs(git_dir1,git_dir2,archive_file)
 
